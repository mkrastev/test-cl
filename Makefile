CXX = g++
CXXFLAGS = -I. -funroll-loops -O3 -DNDEBUG
LINKFLAGS = -lOpenCL
DEPS = scoped.hpp cl_util.hpp
OBJ = main.o cl_util.o
TARGET = test_cl

%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(TARGET): $(OBJ)
	$(CXX) -o $@ $^ $(LINKFLAGS)

.PHONY: clean

clean:
	rm -fv ${OBJ} ${TARGET}
