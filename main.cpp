#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif	
#if CL_VERSION_1_2 == 0
	#error required CL_VERSION_1_2 to compile
#endif
#include <iostream>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "cl_util.hpp"

static const char arg_prefix[] = "-";
static const char arg_discard_platform_version[] = "discard_platform_version";
static const char arg_discard_device_version[] = "discard_device_version";


static int
parse_cli(
	const int argc,
	char** const argv,
	bool& discard_platform_version,
	bool& discard_device_version)
{
	const unsigned prefix_len = std::strlen(arg_prefix);
	bool success = true;

	for (int i = 1; i < argc && success; ++i)
	{
		if (std::strncmp(argv[i], arg_prefix, prefix_len))
		{
			success = false;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_discard_platform_version))
		{
			discard_platform_version = true;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_discard_device_version))
		{
			discard_device_version = true;
			continue;
		}

		success = false;
	}

	if (!success)
	{
		std::cerr << "usage: " << argv[0] << " [<option> ...]\n"
			"options (multiple args to an option must constitute a single string, eg. -foo \"a b c\"):\n"
			"\t" << arg_prefix << arg_discard_platform_version <<
			"\t: discard advertised platform version when collecting platform info\n"
			"\t" << arg_prefix << arg_discard_device_version <<
			"\t\t: discard advertised device version when collectin device info" << std::endl;

		return 1;
	}

	return 0;
}


int
main(
	int argc,
	char** argv)
{
	bool discard_platform_version = false;
	bool discard_device_version = false;

	const int result_cli = parse_cli(
		argc,
		argv,
		discard_platform_version,
		discard_device_version);

	if (0 != result_cli)
		return result_cli;

	const int result_caps = clutil::reportCLCaps(
		discard_platform_version,
		discard_device_version);

	if (0 != result_caps)
		return result_caps;

	return 0;
}
