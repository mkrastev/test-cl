#!/bin/bash
CXX=${CXX:-g++}
TARGET=test_cl
SOURCE=(
	main.cpp
	cl_util.cpp
)
CXXFLAGS=(
	-Wno-logical-op-parentheses
)

if [[ $1 == "debug" ]]; then
	CXXFLAGS+=(
		-Wall
		-O0
		-g
		-DDEBUG
	)
else
	CXXFLAGS+=(
		-funroll-loops
		-O3
		-DNDEBUG
	)
fi

if [[ `uname` == "Darwin" ]]; then
	LINKFLAGS=(
		-framework OpenCL
	)
else
	CXXFLAGS+=(
		-I.
	)
	LINKFLAGS=(
		-lOpenCL
	)
fi
BUILD_CMD=${CXX}" -o "${TARGET}" "${CXXFLAGS[@]}" "${SOURCE[@]}" "${LINKFLAGS[@]}
set -x
${BUILD_CMD}
